﻿
namespace Wp7ChartsSamples.Amcharts.ViewModels
{
    public class ChartItemViewModel
    {
        public double Value { get; set; }

        public double Altitude { get; set; }
    }
}
