﻿using System.Collections.Generic;

namespace Wp7ChartsSamples.Amcharts.ViewModels
{
    public class ChartViewModel
    {
        public List<ChartItemViewModel> Items { get; set; }
    }
}
