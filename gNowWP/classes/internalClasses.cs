﻿using System.Collections.Generic;

namespace gNowWP.classes
{

    public class resultLocation
    {
        public string place_id { get; set; }
        public string licence { get; set; }
        public string osm_type { get; set; }
        public string osm_id { get; set; }
        public List<string> boundingbox { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }
        public string display_name { get; set; }
        public string @class { get; set; }
        public string type { get; set; }
        public double importance { get; set; }
        public string icon { get; set; }
    }

    public class resultData
    {
        public int srtm3 { get; set; }
        public double lng { get; set; }
        public double lat { get; set; }
    }

    public class infoLocation
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
        public int altitude { get; set; }
    }
}
