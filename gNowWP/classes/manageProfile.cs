﻿using System.IO;
using System.IO.IsolatedStorage;
using System.Xml;
using System.Xml.Serialization;

namespace superNova.classes
{
    public class UserInfo
    {
        public string iduser { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public bool metres;
        public bool sync;
        public int ndecimal;
        public int cdecimal;
    }

    public class manageProfile
    {
        private UserInfo data;
        public bool getStatus()
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("user.xml", FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(UserInfo));
                        data = (UserInfo)serializer.Deserialize(stream);
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public UserInfo getUserInfo()
        {
            return data;
        }

        private UserInfo GeneratePersonData()
        {
            UserInfo ui = new UserInfo();

            //asign basic values

            ui.iduser = "";
            ui.name = "";
            ui.metres = true;
            ui.sync = false;
            ui.ndecimal = 3;
            ui.cdecimal = 8;

            return ui;
        }

        public void updateFirstTime()
        {

        }

        public void updateProfile(UserInfo updatedData)
        {
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
            using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                //Open existing file
                if (!myIsolatedStorage.FileExists("user.xml"))
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("user.xml", FileMode.OpenOrCreate))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(UserInfo));
                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            data = GeneratePersonData();
                            serializer.Serialize(xmlWriter, updatedData);
                        }
                    }
                }
            }
        }

        public void createUser()
        {
            //Initialize the session here
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
            using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("user.xml", FileMode.Create))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(UserInfo));
                    using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                    {
                        data = GeneratePersonData();
                        //set values from User
                        serializer.Serialize(xmlWriter, data);
                    }
                }
            }

        }

        public void deleteUser()
        {
            IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForApplication();
            storage.DeleteFile("user.xml");
        }
    }
}
