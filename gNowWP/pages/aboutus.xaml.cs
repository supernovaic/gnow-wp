﻿using System;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;

namespace gNowWP.pages
{
    public partial class aboutus : PhoneApplicationPage
    {
        public aboutus()
        {
            InitializeComponent();
        }

        private void TextBlock_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            EmailComposeTask emailComposeTask = new EmailComposeTask
            {
                To = txtEmail.Text
            };

            emailComposeTask.Show();
        }

        private void TextBlock_Tap_3(object sender, System.Windows.Input.GestureEventArgs e)
        {
            WebBrowserTask wbt = new WebBrowserTask();
            wbt.Uri = new Uri("http://www.sensorsone.com/", UriKind.Absolute);
            wbt.Show();
        }

        private void TextBlock_Tap_4(object sender, System.Windows.Input.GestureEventArgs e)
        {
            WebBrowserTask wbt = new WebBrowserTask();
            wbt.Uri = new Uri("http://www.openstreetmap.org", UriKind.Absolute);
            wbt.Show();
        }

        private void TextBlock_Tap_5(object sender, System.Windows.Input.GestureEventArgs e)
        {
            WebBrowserTask wbt = new WebBrowserTask();
            wbt.Uri = new Uri("http://www.geonames.org/", UriKind.Absolute);
            wbt.Show();
        }

        private void TextBlock_Tap_6(object sender, System.Windows.Input.GestureEventArgs e)
        {
            WebBrowserTask wbt = new WebBrowserTask();
            wbt.Uri = new Uri("http://www.microsoftstudentpartners.com/", UriKind.Absolute);
            wbt.Show();
        }

        private void Image_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            WebBrowserTask wbt = new WebBrowserTask();
            wbt.Uri = new Uri("https://2014.spaceappschallenge.org/awards/#globalawards", UriKind.Absolute);
            wbt.Show();
        }

        private void Image_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            WebBrowserTask wbt = new WebBrowserTask();
            wbt.Uri = new Uri("https://www.nasa.gov", UriKind.Absolute);
            wbt.Show();
        }
    }
}