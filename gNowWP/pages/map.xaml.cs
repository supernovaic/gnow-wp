﻿using System;
using System.Collections.Generic;
using System.Windows;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Maps.Controls;
using gNowWP.ViewModels;
using gNowWP.Resources;

namespace gNowWP.pages
{
    public partial class map : PhoneApplicationPage
    {
        public map()
        {
            InitializeComponent();
        }

        private void createAppBar()
        {
            ApplicationBar = new ApplicationBar();

            ApplicationBarIconButton button1 = new ApplicationBarIconButton();
            ApplicationBarIconButton button2 = new ApplicationBarIconButton();

            button1.IconUri = new Uri("/Assets/AppBar/road.png", UriKind.Relative);
            button1.Text = AppResources.btnRoad;
            ApplicationBar.Buttons.Add(button1);
            button1.Click += new EventHandler(road_Click);

            button2.IconUri = new Uri("/Assets/AppBar/eye.png", UriKind.Relative);
            button2.Text = AppResources.btnAerial;
            ApplicationBar.Buttons.Add(button2);
            button2.Click += new EventHandler(aerial_Click);

            ApplicationBar.Mode = ApplicationBarMode.Minimized;
        }


        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            createAppBar();

            createMap cm = new createMap(gravityLocation);

            sqliteDB cn = new sqliteDB();
            cn.open();

            List<gravityInfo> placeInfo;

            if (NavigationContext.QueryString.ContainsKey("id"))
            {
                lblGLocation.Text = AppResources.lblGLocation;
                string query = "SELECT idgravity, latitude, longitude, altitude, gravity FROM gravityData WHERE idgravity=" + this.NavigationContext.QueryString["id"];

                placeInfo = cn.db.Query<gravityInfo>(query);
            }
            else
            {
                lblGLocation.Text = AppResources.lblGLocation + " " + AppResources.lblOf + ":\n" + this.NavigationContext.QueryString["name"];
                placeInfo = new List<gravityInfo>();
                placeInfo.Add(new gravityInfo() { latitude = double.Parse(this.NavigationContext.QueryString["latitude"]), longitude = double.Parse(this.NavigationContext.QueryString["longitude"]), altitude = int.Parse(this.NavigationContext.QueryString["altitude"]), gravity = double.Parse(this.NavigationContext.QueryString["gravity"]) });
            }

            var values = placeInfo[0];

            lblAltitude.Text = AppResources.lblAltitude2 + " (m):\n" + values.altitude.ToString();
            lblLatitude.Text = AppResources.lblLatitude + ":\n" + Math.Round(values.latitude, 6).ToString();
            lblLongitude.Text = AppResources.lblLongitude + ":\n" + Math.Round(values.longitude, 6).ToString();
            lblGravity.Text = AppResources.lblGravity3 + "\n" + Math.Round(values.gravity, 6).ToString();

            cm.setCenter(values.latitude, values.longitude, 13);

            List<Tuple<double, double>> locations = new List<Tuple<double, double>>();
            locations.Add(new Tuple<double, double>(values.latitude, values.longitude));

            cm.addPushpins(locations);

            cn.close();
        }

        private void road_Click(object sender, EventArgs e)
        {
           gravityLocation.CartographicMode = MapCartographicMode.Road;
        }

        private void aerial_Click(object sender, EventArgs e)
        {
            gravityLocation.CartographicMode = MapCartographicMode.Aerial;
        }

        private void btnZoomIn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            double currentZoom = gravityLocation.ZoomLevel;
            if (currentZoom < 20)
                gravityLocation.ZoomLevel = gravityLocation.ZoomLevel + 0.5;
        }

        private void btnZoomOut_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            double currentZoom = gravityLocation.ZoomLevel;
            if (currentZoom > 0)
                gravityLocation.ZoomLevel = gravityLocation.ZoomLevel - 0.5;
        }

        private void gravityLocation_Loaded(object sender, RoutedEventArgs e)
        {
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.ApplicationId = "GNow";
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.AuthenticationToken = "ApdHCFhb4A9suS7wDOR_7WvISz9MQXeKifw2fTLBzhGywvrPrSsoh8R3YYDtCjKd";
        }
    }
}