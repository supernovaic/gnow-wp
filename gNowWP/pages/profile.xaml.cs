﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Reactive;
using Newtonsoft.Json;
using gNowWP.Resources;
using superNova.classes;
using gNowWP.ViewModels;

namespace gNowWP.pages
{
    public partial class settings : PhoneApplicationPage
    {
        private class ItemData
        {
            public string axis { get; set; }
            public double value { get; set; }
        }

        private UserInfo _data;
        private manageProfile _profile;
        private cnnStatus _cnnstatus;

        public settings()
        {
            InitializeComponent();
            _profile = new manageProfile();
            _cnnstatus = new cnnStatus();
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml?r=" + Guid.NewGuid().ToString(), UriKind.Relative));
        }

        private void updateUser()
        {
            _profile.updateProfile(GeneratePersonData(_data.iduser));
        }

        private UserInfo GeneratePersonData(string uid)
        {
            UserInfo ui = new UserInfo();

            //asign basic values
            ui.iduser = uid;

            if (cmbUnits.SelectedIndex == 0)
                ui.metres = true;
            else
                ui.metres = false;

            //            ui.sync = (bool)btnSync.IsChecked;

            ui.ndecimal = (int)sldGravity.Value;

            ui.cdecimal = (int)sldCalculator.Value;

            return ui;
        }

        private void cmbUnits_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(pvtOptions == null))
                updateUser();
        }

        /*        private void btnSync_Click(object sender, RoutedEventArgs e)
                {
                    if (!(pvtOptions == null))
                        updateUser();
                }*/

        private void sldGravity_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!(pvtOptions == null))
            {
                sldGravity.Value = Math.Round(e.NewValue);
                updateUser();
            }
        }

        private void sldCalculator_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!(pvtOptions == null))
            {
                sldCalculator.Value = Math.Round(e.NewValue);
                updateUser();
            }
        }

        private void myLocations_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(pvtOptions == null))
            {
                LongListSelector item = (LongListSelector)sender;
                if (item.SelectedItem != null)
                {
                    gravityInfo p = item.SelectedItem as gravityInfo;

                    string url = "/pages/map.xaml?id=" + p.idgravity.ToString();
                    NavigationService.Navigate(new Uri(url, UriKind.Relative));
                }
            }
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            sqliteDB cn = new sqliteDB();
            cn.open();

            string query = "SELECT idgravity, round(latitude,6) latitude, round(longitude,6) longitude, altitude, round(gravity,6) gravity FROM gravityData ORDER BY registered DESC";
            List<gravityInfo> placeInfo = cn.db.Query<gravityInfo>(query);
            myLocations.ItemsSource = placeInfo;

            var mProfile = new manageProfile();

            _data = mProfile.getUserInfo();

            if (_data == null)
            {
                mProfile.createUser();
                _data = mProfile.getUserInfo();
            }
            cn.close();
        }

        private void syncButton_Click(object sender, EventArgs e)
        {
            getLocation();
        }

        private void getLocation()
        {
            sqliteDB cn = new sqliteDB();
            cn.open();

            string query = "SELECT idgravity, latitude, longitude, altitude, gravity, status FROM gravityData WHERE status='false' LIMIT 1";
            List<gravityInfo> placeInfo = cn.db.Query<gravityInfo>(query);

            if (placeInfo.Count > 0)
            {
                setGravity("true", placeInfo[0].latitude, placeInfo[0].longitude, placeInfo[0].altitude);
                setLocation(placeInfo[0].latitude, placeInfo[0].longitude, placeInfo[0].altitude, placeInfo[0].gravity);
            }
            else if (placeInfo.Count == 0)
                MessageBox.Show(AppResources.successFull, AppResources.titleThanks, MessageBoxButton.OK);
        }

        private void setLocation(double latitude, double longitude, double altitude, double gravity)
        {
            if (!string.IsNullOrEmpty(_data.iduser))
            {

                string URL = "http://gnow.hostingsiteforfree.com/webServices/setLocation.php?uid=" + _data.iduser + "&altitude=" + altitude.ToString() + "&longitude=" + longitude.ToString() + "&latitude=" + latitude.ToString() + "&gravity=" + gravity.ToString();

                cleanString cs = new cleanString();

                WebClient w = new WebClient();

                Observable
                .FromEvent<DownloadStringCompletedEventArgs>(w, "DownloadStringCompleted")
                .Subscribe(r =>
                {
                    var deserialized = JsonConvert.DeserializeObject<List<result>>(cs.clean(r.EventArgs.Result));

                    switch (deserialized[0].total)
                    {
                        case "0":
                            Console.Write(AppResources.errReg, "Error", MessageBoxButton.OK);
                            break;
                        case "2":
                            MessageBox.Show(AppResources.errTry, "Error", MessageBoxButton.OK);
                            break;
                        default:
                            getLocation();
                            break;
                    }
                });
                w.DownloadStringAsync(
                new Uri(URL));
            }
            else
                MessageBox.Show(AppResources.errLogin, "Error", MessageBoxButton.OK);
        }

        private void setGravity(string cStatus, double latitude, double longitude, double altitude)
        {
            gravity G = new gravity();
            sqliteDB cn = new sqliteDB();
            cn.open();

            string query = "SELECT idgravity, latitude, longitude, altitude, gravity FROM gravityData WHERE latitude=" + latitude.ToString() + " AND longitude=" + longitude.ToString() + " AND altitude=" + altitude.ToString();

            var existing = cn.db.Query<gravityData>(query).FirstOrDefault();
            if (existing != null)
            {
                existing.status = cStatus;

                cn.db.RunInTransaction(() =>
                {
                    cn.db.Update(existing);
                });
            }
            cn.close();
        }
    }
}